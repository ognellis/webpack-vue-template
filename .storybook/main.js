const path = require('path');

module.exports = {
    stories: ['../src/**/*.stories.js'],
    webpackFinal: async (config, { configType }) => {
        config.module.rules.push({
            test: /\.scss$/,
            use: ['style-loader', 'css-loader', 'sass-loader'],
            include: path.resolve(__dirname, '../'),
        });
        config.resolve = {
            ...config.resolve,
            alias: {
                ...config.resolve.alias,
                src: path.resolve(__dirname, '../src/'),
            },
        };
        return config;
    },
};