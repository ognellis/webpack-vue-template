const webpack = require('webpack');
const path = require('path');
const merge = require('webpack-merge');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
// const HtmlWebpackPlugin = require('html-webpack-plugin');
const VueSSRClientPlugin = require('vue-server-renderer/client-plugin');
const baseConfig = require('./webpack.config.base.js');

const isProduction = process.env.NODE_ENV === 'production';

let config = merge(baseConfig, {
    mode: 'development',
    entry: './src/client/entry-client.js',
    devtool : 'cheap-eval-source-map',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].[hash:8].js',
        publicPath: '/dist/',
      },
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        hot: true,
        port: 8080
    },
    plugins: [
        new VueSSRClientPlugin()
    ],
});

if (!isProduction) {
    config = merge(config, {
        output: {
        filename: '[name].js',
        publicPath: 'http://localhost:9999/dist/',
        },
        plugins: [new webpack.HotModuleReplacementPlugin()],
        devtool: 'source-map',
        devServer: {
        writeToDisk: true,
        contentBase: path.resolve(__dirname, 'dist'),
        publicPath: 'http://localhost:9999/dist/',
        hot: true,
        inline: true,
        historyApiFallback: true,
        port: 9999,
        headers: {
            'Access-Control-Allow-Origin': '*',
        },
        },
    });
} else {
    config = merge(config, {
        plugins: [
        new MiniCssExtractPlugin({
            filename: '[name].[hash:8].css',
        }),
        ],
    });
}

module.exports = config;
