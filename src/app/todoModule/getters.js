export const getters = {
    todoList: state => {
        return state.todoList;
    },
    lastId: state => {
        return state.lastId;
    },
};