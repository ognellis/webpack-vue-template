export const mutations = {
    ADD_TASK(state, id) {
        const newId = id+1;
        state.todoList = [
            {
                id: newId,
                text: 'Введите название',
                isDone: false,
            },
            ...state.todoList,
        ];
        state.lastId = newId;
    },
    CHANGE_TASK_TEXT(state, { value, id }) {
        const targetIndex = state.todoList.findIndex(item => item.id === id);

        state.todoList = [
            ...state.todoList.slice(0, targetIndex),
            {
                ...state.todoList[targetIndex],
                text: value,
            },
            ...state.todoList.slice(targetIndex+1),
        ];
    },
    CHANGE_TASK_STATUS(state, { value, id }) {
        const targetIndex = state.todoList.findIndex(item => item.id === id);

        state.todoList = [
            ...state.todoList.slice(0, targetIndex),
            {
                ...state.todoList[targetIndex],
                isDone: value,
            },
            ...state.todoList.slice(targetIndex+1),
        ];
    },
};
