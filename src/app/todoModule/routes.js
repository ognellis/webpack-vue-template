import TodoPage from './pages/TodoList.vue';

export const routes = [
  { path: '/', component: TodoPage },
];