import { actions } from './actions';
import { mutations } from './mutations';
import { getters } from './getters';

export const todoModule = {
    namespaced: true,
    state: () => ({
        lastId: 4,
        todoList: [
            {
                id: 4,
                text: 'Задачка4dsadasd из стейта',
                isDone: false,
            },
            {
                id: 3,
                text: 'Задачка3 из стейта',
                isDone: false,
            },
            {
                id: 2,
                text: 'Задачка2 из стейта',
                isDone: false,
            },
            {
                id: 1,
                text: 'Задачка1 из стейта',
                isDone: true,
            },
        ],
    }),
    getters,
    mutations,
    actions,
};