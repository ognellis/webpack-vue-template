import { getters } from './getters';

describe('todoModule getters', () => {
    const stateMock = {
        todoList: [
            {
                id: 4,
                text: 'Задачка4 из стейта',
                isDone: false,
            },
        ],
        lastId: 4
    };
    it('todoList should return correct value', () => {
        expect(getters.todoList(stateMock)).toEqual(stateMock.todoList);
    });

    it('lastId should return correct value', () => {
        expect(getters.lastId(stateMock)).toBe(4);
    });
});