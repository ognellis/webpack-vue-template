import { shallowMount, createLocalVue } from '@vue/test-utils';
import TodoItem from './TodoItem.vue';

describe('TodoItem.vue', () => {
    const wrapperFactory = ({ props, data } = {}) => {
        const localVue = createLocalVue();
        return shallowMount(TodoItem, {
            localVue,
            propsData: {
                text: 'Задачка',
                isDone: false,
                ...props,
            },
            data() {
                return {
                    iconFill: '#ffffff',
                    isEditMode: false,
                    ...data,
                };
            },
            stubs: {
                'text-area-component': true,
                'tick-icon': true,
            },
        });
    };

    beforeEach(() => {

    });

    it('should render correctly', () => {
        const wrapper = wrapperFactory();
        expect(wrapper.element).toMatchSnapshot();
    });

    it('should render textArea if isEditMode is true', () => {
        const wrapper = wrapperFactory({ data: { isEditMode: true } });
        const textArea = wrapper.find('text-area-component-stub');
        const content = wrapper.find('.todo-item__content');
        expect(textArea.exists()).toBeTruthy();
        expect(content.exists()).toBeFalsy();
        expect(wrapper.element).toMatchSnapshot();
    });

    it('shouldn\'t render textArea if isEditMode is false', () => {
        const wrapper = wrapperFactory({ data: { isEditMode: false } });
        const textArea = wrapper.find('text-area-component-stub');
        const content = wrapper.find('.todo-item__content');
        expect(textArea.exists()).toBeFalsy();
        expect(content.exists()).toBeTruthy();
        expect(wrapper.element).toMatchSnapshot();
    });

    it('should show tick-icon if task is done', () => {
        const wrapper = wrapperFactory({ props: { isDone: true } });
        const tickIcon = wrapper.find('tick-icon-stub');
        expect(tickIcon.exists() && tickIcon.isVisible()).toBeTruthy();
    });

    it('shouldn\'t show tick-icon if task is done', () => {
        const wrapper = wrapperFactory({ props: { isDone: false } });
        const tickIcon = wrapper.find('tick-icon-stub');
        expect(tickIcon.isVisible()).toBeFalsy();
    });

    it('should emit change-status action then status button clicked', () => {
        const wrapper = wrapperFactory();
        const statusButton = wrapper.find('.todo-item__state');
        statusButton.trigger('click');
        expect(wrapper.emitted()['change-status']).toBeTruthy();
    });
});