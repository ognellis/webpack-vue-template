import TodoItem from './TodoItem.vue';

export default { title: 'TodoItem' };

export const interactive = () => ({
    components: { TodoItem },
    data() {
        return {
            isDone: false,
            text: 'Задачка',
        };
    },
    methods: {
        changeState(newState) {
            this.isDone = newState;
        },
        changeText(value) {
            this.text = value;
        },
    },
    template: '<todo-item style="width: 300px;" :is-done="isDone" @change-text="changeText" @change-state="changeState" :text="text"/>'
});

export const bigText = () => ({
    components: { TodoItem },
    data() {
        return {
            isDone: false,
            text: 'Офигенно огромное описание задачи ипсум долор, задача задача задача, надо чтобы несколько строк было, прям чтобы хотябы 3 строки',
        };
    },
    methods: {
        changeState(newState) {
            this.isDone = newState;
        },
        changeText(value) {
            this.text = value;
        },
    },
    template: '<todo-item style="width: 300px;" :is-done="isDone" @change-text="changeText" @change-state="changeState" :text="text" />'
  });

export const doneState = () => ({
  components: { TodoItem },
  template: '<todo-item style="width: 300px;" :is-done="true" text="Задачка"/>'
});

export const notDoneState = () => ({
  components: { TodoItem },
  template: '<todo-item style="width: 300px;" :is-done="false" text="Задачка"/>'
});

