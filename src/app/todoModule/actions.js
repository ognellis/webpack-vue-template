export const actions = {
    addTask({ commit, getters }) {
        commit('ADD_TASK', getters.lastId);
    },
    changeTaskText({ commit }, { value, id }) {
        commit('CHANGE_TASK_TEXT', { value, id });
    },
    changeTaskStatus({ commit }, { value, id }) {
        commit('CHANGE_TASK_STATUS', { value, id });
    },
};
