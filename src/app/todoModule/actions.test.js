import { actions } from './actions';

describe('todoModule actions.js', () => {
    it('addTask should call ADD_TASK mutation', () => {
        const context = {
            commit: jest.fn(),
            getters: { lastId: 1 },
        };
        actions.addTask(context);
        expect(context.commit).toHaveBeenCalledWith('ADD_TASK', 1);
    });

    it('changeTaskText should call CHANGE_TASK_TEXT mutation', () => {
        const context = {
            commit: jest.fn(),
        };
        const payload = {
            value: 'value',
            id: 'id',
        };
        actions.changeTaskText(context, payload);
        expect(context.commit).toHaveBeenCalledWith('CHANGE_TASK_TEXT', { value: 'value', id: 'id' });
    });

    it('changeTaskStatus should call CHANGE_TASK_STATUS mutation', () => {
        const context = {
            commit: jest.fn(),
        };
        const payload = {
            value: 'value',
            id: 'id',
        };
        actions.changeTaskStatus(context, payload);
        expect(context.commit).toHaveBeenCalledWith('CHANGE_TASK_STATUS', { value: 'value', id: 'id' });
    });
});