import Vuex from 'vuex';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import { getters } from '../getters';
import TodoList from './TodoList.vue';

describe('TodoList.vue', () => {
    let store;
    const wrapperFactory = () => {
        const localVue = createLocalVue();
        localVue.use(Vuex);
        store = new Vuex.Store({
            modules: {
                todo: {
                    namespaced: true,
                    state: {
                        lastId: 4,
                        todoList: [
                            {
                                id: 4,
                                text: 'Задачка4 из стейта',
                                isDone: false,
                            },
                            {
                                id: 3,
                                text: 'Задачка3 из стейта',
                                isDone: false,
                            },
                            {
                                id: 2,
                                text: 'Задачка2 из стейта',
                                isDone: false,
                            },
                            {
                                id: 1,
                                text: 'Задачка1 из стейта',
                                isDone: true,
                            },
                        ],
                    },
                    getters,
                    actions: {
                        addTask: jest.fn(),
                        changeTaskText: jest.fn(),
                        changeTaskStatus: jest.fn(),
                    },
                },
            },

        });

        return shallowMount(TodoList, {
            store,
            localVue,
        });
    };

    it('should render correctly', () => {
        const wrapper = wrapperFactory();
        expect(wrapper.element).toMatchSnapshot();
    });
});