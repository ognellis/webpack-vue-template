import { mutations } from './mutations';

describe('todoModule mutations.js', () => {
    let state;

    beforeEach(() => {
        state = {
            todoList: [
                {
                    id: 3,
                    text: 'Задача 3',
                    isDone: false,
                },
                {
                    id: 2,
                    text: 'Задача 2',
                    isDone: false,
                },
                {
                    id: 1,
                    text: 'Задача 1',
                    isDone: false,
                },
            ],
            lastId: 3,
        };
    });

    it('ADD_TASK', () => {
        mutations.ADD_TASK(state, 3);
        expect(state.lastId).toBe(4);
        expect(state.todoList.length).toBe(4);
        expect(state.todoList[0]).toEqual({
            id: 4,
            text: 'Введите название',
            isDone: false,
        });
    });

    it('CHANGE_TASK_TEXT', () => {
        const payload = {
            value: 'Новый текст',
            id: 2,
        };
        mutations.CHANGE_TASK_TEXT(state, payload);
        const targetTask = state.todoList.find(task => task.id === 2);
        expect(targetTask.text).toBe('Новый текст');
    });

    it('CHANGE_TASK_STATUS', () => {
        const payload = {
            value: true,
            id: 2,
        };
        mutations.CHANGE_TASK_STATUS(state, payload);
        const targetTask = state.todoList.find(task => task.id === 2);
        expect(targetTask.isDone).toBeTruthy();
    });
});