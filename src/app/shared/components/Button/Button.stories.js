import VueButton from './Button.vue';

export default { title: 'Button' };

export const withText = () => ({
  components: { VueButton },
  template: '<vue-button>Новая задача</vue-button>'
});

export const withEmoji = () => ({
  components: { VueButton },
  template: '<vue-button>😀 😎 👍 💯</vue-button>'
});