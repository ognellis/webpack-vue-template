import VueInput from './Input.vue';

export default { title: 'Button' };

export const standard = () => ({
    components: { VueInput },
    template: '<vue-input>title</vue-input>'
});