import VueRouter from 'vue-router';
import { routes as todoRoutes } from './todoModule/routes';

const routes = [
    ...todoRoutes,
];

export const router = new VueRouter({
    mode: 'history',
    routes,
});