import Vue from 'vue';
import Vuex from 'vuex';
import { todoModule } from '@/todoModule/module';

Vue.use(Vuex);

export const store = new Vuex.Store({
    modules: {
        todo: todoModule,
    },
});
