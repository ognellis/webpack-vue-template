import Vue from 'vue';
import App from '../app/baseModule/components/App.vue';
import { router } from '../app/router';
import VueRouter from 'vue-router';
import VueMeta from 'vue-meta';
import { store } from './store';

Vue.use(VueRouter);
Vue.use(VueMeta);

export const createApp = () => {
    const app = new Vue({
        router,
        store,
        render: (h) => h(App),
    });
    return { app, router, store };
};
