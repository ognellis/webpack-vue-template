const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const StylelintPlugin = require('stylelint-webpack-plugin');

module.exports = {
    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            },
            {
                test: /\.js$/,
                use: ['babel-loader', 'eslint-loader'],
                exclude: /node_modules/
            },
            {
                test: /\.s[ac]ss$/i,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'sass-loader',
                ],
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: [
                    'file-loader',
                ],
            },
        ],
      },
    plugins: [
        // new CleanWebpackPlugin(),
        new MiniCssExtractPlugin(),
        new VueLoaderPlugin(),
        new StylelintPlugin({
            files: ['src/**/*.{vue,htm,html,css,sss,less,scss,sass}'],
        }),
    ],
    resolve: {
        alias: {
            src: path.resolve(__dirname, 'src/'),
            '@': path.resolve(__dirname, 'src/app/'),
        },
    },
};